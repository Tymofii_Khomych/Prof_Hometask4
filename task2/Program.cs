﻿using System.Text.RegularExpressions;
using System.IO;

namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var validUrl = new Regex("https?://(?:www\\.)?[-a-zA-Z0-9@:%._\\+~#=]+\\.[a-zA-Z0-9()]{2,6}(?:[-a-zA-Z0-9()@:%_\\+.~#?&//=]{2,6})");
            var validPhoneNumber = new Regex("\\+?[1-9][0-9]{7,14}");
            var validEmail = new Regex(@"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}");

            //string someText = "JDfadfaafafda1231dafkadfahdfa@gmail.comsadass12312312фвафhttps://hello.comadsasdak12312a+381231212312фффф";

            //List<string> matches = new List<string>();

            //foreach (Match match in validUrl.Matches(someText))
            //{
            //    matches.Add(match.Value);
            //}

            //foreach (Match match in validPhoneNumber.Matches(someText))
            //{
            //    matches.Add(match.Value);
            //}

            //foreach (Match match in validEmail.Matches(someText))
            //{
            //    matches.Add(match.Value);
            //}

            //Console.WriteLine("Matches found:");
            //foreach (string match in matches)
            //{
            //    Console.WriteLine(match);
            //}

            string url = "https://www.digikala.com/";

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = client.GetAsync(url).Result)
                {
                    using (HttpContent content = response.Content)
                    {
                        string result = content.ReadAsStringAsync().Result;

                        Console.WriteLine(result + "\n\n\n");
                        
                        List<string> matches = new List<string>();

                        foreach (Match match in validUrl.Matches(result))
                        {
                            matches.Add(match.Value);
                        }

                        foreach (Match match in validPhoneNumber.Matches(result))
                        {
                            matches.Add(match.Value);
                        }

                        foreach (Match match in validEmail.Matches(result))
                        {
                            matches.Add(match.Value);
                        }

                        using (var file = new FileStream("result.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite))
                        {
                            using (var writer = new StreamWriter(file))
                            {
                                Console.WriteLine("Matches found:");
                                foreach (string match in matches)
                                {
                                    Console.WriteLine(match);
                                    writer.WriteLine(match);
                                }
                            }
                        }
                    }
                }
            }
        }

    }
}