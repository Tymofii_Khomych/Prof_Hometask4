﻿using System.Text;
using System.Text.RegularExpressions;

namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            string filePath = "text.txt";
            string fileContent = File.ReadAllText(filePath);
            Console.WriteLine(fileContent);

            string pattern = @"\b\w+е\b|\b\w+ий\b|\b\w+а\b|\b\w+ій\b";
            MatchCollection matches = Regex.Matches(fileContent, pattern);
            Console.WriteLine("\n------------------------------------------------------------\n");

            Console.WriteLine("Прикметники:");
            foreach (Match match in matches)
            {
                Console.WriteLine(match.Value);
                
            }

            string replacedContent = Regex.Replace(fileContent, pattern, match => "Гав!");

            Console.WriteLine("\n------------------------------------------------------------\n");
            Console.WriteLine("Replaced content:");
            Console.WriteLine(replacedContent);

            File.WriteAllText(filePath, replacedContent);
        }
    }
}