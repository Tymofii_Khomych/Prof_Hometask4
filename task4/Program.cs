﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace task4
{
    class Program
    {
        static void AddItemToCheck(StreamWriter writer, string itemName, double itemPrice)
        {
            string formattedPrice = itemPrice.ToString("C", CultureInfo.CurrentCulture);
            writer.WriteLine(itemName + " – " + formattedPrice);
        }

        static void Main()
        {
            Console.OutputEncoding = Encoding.Unicode;

            var userCulture = CultureInfo.CurrentCulture;
            Console.WriteLine("Локаль користувача: " + userCulture.DisplayName);

            string filePath = "check.txt";
            using (StreamWriter writer = new StreamWriter(filePath, false))
            {
                AddItemToCheck(writer, "Ноутбук", 25000.0);
                AddItemToCheck(writer, "Смартфон", 12000.0);
                AddItemToCheck(writer, "Фотокамера", 15000.0);
            }

            Console.WriteLine("Інформація з чека (" + userCulture.DisplayName + "):");
            Console.WriteLine(File.ReadAllText(filePath));

            var enUsCulture = new CultureInfo("en-US");
            Console.WriteLine("Локаль en-US: " + enUsCulture.DisplayName);

            Console.WriteLine("Інформація з чека (en-US):");
            string checkContentEnUs = File.ReadAllText(filePath);
            Console.WriteLine(checkContentEnUs);
        }
    }
}