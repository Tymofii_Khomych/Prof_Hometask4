﻿using System.Text.RegularExpressions;
namespace task6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Login: ");
            string login = Console.ReadLine();
            Console.Write("Password: ");
            string password = Console.ReadLine();

            var validLogin = new Regex("^[a-zA-z]+$");
            var validPassword = new Regex("^[a-zA-z0-9]+$");

            if(validLogin.IsMatch(login) && validPassword.IsMatch(password)) 
            {
                Console.WriteLine("Authorization successful");
            }
            else
            {
                Console.WriteLine("Authorization is not successful");
            }
        }
    }
}